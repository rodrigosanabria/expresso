import request from 'supertest';
import { app } from '../src/app';

describe( "Test Get /", () => {
    test('It should response the GET method', async () => {
        const response = await request(app).get('/');
        expect(response.status).toBe(200);
    })
}
)

describe("No token", () => {
    test('It should response with error', async() => {
        const response = await request(app).get('/services/contact');
        expect(response.status).toBe(400);
    })
})

describe("Test service routes", () => {
})