import express from 'express';
import bodyParser from 'body-parser';
import { mainRouter } from './main/main.routes';
import cors from 'cors';
import morgan from 'morgan';
export const app = express();

app.use(bodyParser.json())
app.use(morgan('tiny'))
app.use(cors())

/* app.use("*", (req,res,next) => {
    if ( req.headers["x-access-token"] === "something") {
        next()
    } else {
        res.status(404).json({message: "invalid token."})
    }
}) */

app.use('/', mainRouter);