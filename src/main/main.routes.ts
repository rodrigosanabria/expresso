import { Router, Application } from 'express';
import { getVersion} from './main.ctrl';
export const mainRouter = Router();

mainRouter
.get('', getVersion)